
#include <vector>
#include <string>

#ifndef __cplusplus
#error "This is a C++ file. Must be compiled in C++ mode."
#endif

extern "C" {
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <unistd.h>
#include "image_u8.h"
}

class DetectorWrapper;
struct detection {
  int family_size; // one of 36 -> tag36h11
                   //        25 -> tag25h9
                   //        16 -> tag16h5
                   // other tag families are NOT supported!
  int id;          // the number of the identified tag
  int hamming;     // hamming distance from true tag. higher is less likely to
                   // be a true detection. 0 is best.
  float cx, cy;    // floating point coordinates of the location of the tag, in
                   // pixels. +x is right, +y is down.
  float corners[8];// corners of the tag in pixels: [x0, y0, x1, y1, ...]
  float homography[9]; // corners of the tag in pixels: [x0, y0, x1, y1, ...]
};

// start up and load tag families, etc.
DetectorWrapper* initializeDetectorWrapper();

// Detects tags from all supported families. Will be slow.
std::vector<detection> detectTags(DetectorWrapper* detector, image_u8_t* image);



//void annotateTags(std::vector<detection>, image_color_t* image);
//std::string prettyPrintDetection(const detection& d);
