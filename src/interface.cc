
#include "interface.h"


#include "apriltag.h"
#include "tag36h11.h"
#include "tag25h9.h"
#include "tag16h5.h"

class DetectorWrapper {
  public:
    apriltag_detector_t* td;
};

DetectorWrapper* initializeDetectorWrapper() {
    auto detector = new DetectorWrapper();
    detector->td = apriltag_detector_create();
    
    apriltag_family_t *tf = NULL;
    tf = tag36h11_create();
    tf->black_border = 1;
    apriltag_detector_add_family(detector->td, tf);
    tf = tag16h5_create();
    tf->black_border = 1;
    apriltag_detector_add_family(detector->td, tf);
    tf = tag25h9_create();
    tf->black_border = 1;
    apriltag_detector_add_family(detector->td, tf);
    
    
    detector->td->debug = false;
    detector->td->quad_decimate = 1.0;
    detector->td->quad_sigma = 0;
    detector->td->nthreads = 4;
    detector->td->refine_edges = 0;
    detector->td->refine_decode = 0;
    detector->td->refine_pose = 0;
    
    return detector;
}

std::vector<detection> detectTags(DetectorWrapper* detector, image_u8_t* image) {
  zarray_t *detections = apriltag_detector_detect(detector->td, image);
  std::vector<detection> detects{};
  for (int i = 0; i < zarray_size(detections); i++) {
      apriltag_detection_t *det;
      zarray_get(detections, i, &det);
      detection d;
      d.family_size = det->family->d*det->family->d;
      d.id = det->id;
      d.hamming = det->hamming;
      d.cx = det->c[0];
      d.cy = det->c[1];
      for (int i = 0; i < 4; i++) {
        d.corners[2*i+0] = det->p[i][0];
        d.corners[2*i+1] = det->p[i][1];
      }
      for (int i = 0; i < 9; i++) {
        d.homography[i] = (float)det->H->data [i];
      }
      detects.push_back(d);
  }
  apriltag_detections_destroy(detections);

  return detects;
}
