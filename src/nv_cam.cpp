#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/CameraInfo.h>
#include <read_cam/detection.h>
#include <stdio.h>
#include <malloc.h>
#include "djicam.h"
#include "interface.h"

#define IMAGE_W 1280
#define IMAGE_H 720
#define FRAME_SIZE              IMAGE_W*IMAGE_H*3

unsigned char buffer[FRAME_SIZE] = {0};

int main(int argc, char **argv)
{
	ros::init(argc,argv,"image_raw");
	
	unsigned int frame_number;
	
	int to_mobile = 0;
	int mode = GETBUFFER_MODE;

	ros::NodeHandle nh_private("~");
	nh_private.param("to_mobile", to_mobile, 0);

	if(to_mobile) {
		mode |= TRANSFER_MODE;
	}
	
	if(manifold_cam_init(mode) == -1) {
		printf("Error in manifold_cam_init()\n");
		return -1;
	}
	
	ros::NodeHandle node{};
	
	ros::Publisher pub = node.advertise<read_cam::detection>("apriltag_detection", 100);
	
	auto detector = initializeDetectorWrapper();
	
	while (true) {
		int ret = manifold_cam_read(buffer, &frame_number, 1);
    ros::Time capture_time = ros::Time::now();
		
    if (ret == -1)
      break;
    
    ROS_INFO("Processing Frame %d", frame_number);
		image_u8_t image;
		image.width = 1280;
		image.height = 720;
		image.stride = image.width;
		image.buf = buffer;
		std::vector<detection> detections = detectTags(detector, &image);

    for (auto & detect : detections) {
      if (detect.hamming == 0) {
        read_cam::detection detect_msg;
				detect_msg.timestamp = capture_time;
				detect_msg.family_size = detect.family_size;
				detect_msg.id          = detect.id;
				detect_msg.hamming     = detect.hamming;
				detect_msg.cx = detect.cx;
        detect_msg.cy = detect.cy;
				for(int i = 0; i < 8; i++)
					detect_msg.corners[i] = detect.corners[i];
				for(int i = 0; i < 9; i++)
					detect_msg.homography[i] = detect.homography[i];
				
				ROS_INFO("Detection of id %d t %f, %f", detect.id, detect.cx, detect.cy);
        pub.publish(detect_msg);
      }
    }
    
		ros::spinOnce();
    usleep(10);
	}
	while(!manifold_cam_exit()) {
		usleep(1000);
	}
	return 0;
}
